Pod::Spec.new do |s|
  s.name         = "SportsTalk_iOS_SDK"
  s.version      = "1.0.0"
  s.summary      = "The Sportstalk SDK is a helpful wrapper around the Sportstalk API"
  s.description  = <<-DESC
  The Sportstalk SDK is a helpful wrapper around the Sportstalk API.
                   DESC
  s.homepage     = "http://shaped-entropy-259212.appspot.com/demo"
  s.license      = "Copyleft"
  s.author       = { "Junda" => "Mr. Parv Hanikarak" }
  s.source       = { :path => '.' }
  # s.source       = { :git => "https://gitlab.com/minakshib/sportstalk_ios_sdk.git", :tag => "#{s.version}" }
  s.source_files  = "Source/**/*.swift"
end