import XCTest
import SportsTalk_iOS_SDK

let RESPONSE_PARAMETER_DATA = "data"
let RESPONSE_PARAMETER_USERS = "users"
let RESPONSE_PARAMETER_CODE = "code"

class SportsTalk_iOS_SDKTests: XCTestCase {
    
    let services = Services()
    
    let Expectation_Description = "Testing"
    let TEST_USERID = "046282d5e2d249739e0080a4d2a04904"
    let TEST_HANDLE = "@test"
    let TEST_BANNED = true
    let TEST_DESCRIPTION = "Testing"
    let TEST_DISPLAY_NAME = "Test User"
    let TEST_PICTURE_URL = URL(string: "www.sampleUrl1.com")
    let TEST_PROFILE_URL = URL(string: "www.sampleUrl1.com")
    let TEST_WEBHOOK_ID = "046282d5e2d249739e0080a4d2a04904"
    let TEST_COMMAND = "hello_world" //"*help"
    let TEST_ROOM_NEWEST_EVENT_ID = "5e0ec9b138a2831098526cac"
    let TEST_CHAT_ROOM_ID = "5e0ec9b138a2831098526cac" //"1935"//
    let TEST_ROOM_ID_OR_SLUG = "5e0ec9b138a2831098526cac"
    let TEST_CUSTOM_EVENT = "123456"
    let TEST_REPLY_TO = "5e0ec9b138a2831098526cac"
    let TEST_REACTION = "Like"
    let TEST_REACTED = true
    let TEST_CUSTOM_ID = "5e0ec9b138a2831098526cac"
    let TEST_CUSTOM_PAYLOAD = "test 12345"
    let TEST_ROOM_ID_OR_LABEL = ""
    let TEST_EVENT_ID = ""
    let TEST_CHAT_MESSAGE_ID = ""
    let TEST_CURSOR = "3"
    let TEST_SLUG = URL(string: "www.sampleUrl2.com")
    let TEST_ENABLEACTIONS = true
    let TEST_ENABLEENTERANDEXIT = false
    let TEST_ROOMISOPEN = false
    let TEST_THROTTLE = 0
    
    let RESPONSE_PARAMETER_DISPLAY_NAME = "displayname"
    let RESPONSE_PARAMETER_PROFILE_URL = "profileurl"
    let RESPONSE_PARAMETER_PICTURE_URL = "pictureurl"
    
    
    override func setUp() {
        //        services.url = URL(string: "http://shaped-entropy-259212.appspot.com/demo/api/v3")
        services.url = URL(string: "http://api-origin.sportstalk247.com/api/v3")
        services.authToken = "vfZSpHsWrkun7Yd_fUJcWAHrNjx6VRpEqMCEP3LJV9Tg"
    }
    
    func test_UserServices_UpdateUser()
    {
        let createUser = UsersServices.CreateUpdateUser()
        createUser.userid = TEST_USERID
        createUser.handle = TEST_HANDLE
        createUser.displayname = TEST_DISPLAY_NAME
        createUser.pictureurl = TEST_PICTURE_URL
        createUser.profileurl = TEST_PROFILE_URL

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.usersServices(createUser) { (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertEqual(TEST_DISPLAY_NAME, responseData?[RESPONSE_PARAMETER_DISPLAY_NAME] as? String)
        XCTAssertEqual(TEST_PROFILE_URL?.absoluteString, responseData?[RESPONSE_PARAMETER_PROFILE_URL] as? String)
        XCTAssertEqual(TEST_PICTURE_URL?.absoluteString, responseData?[RESPONSE_PARAMETER_PICTURE_URL] as? String)
    }

//    func test_UserServices_GetUserDetails()
//    {
//        let getUserDetail = UsersServices.GetUserDetails()
//        getUserDetail.userid = TEST_USERID
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//
//        services.ams.usersServices(getUserDetail) { (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertEqual(TEST_DISPLAY_NAME, responseData?[RESPONSE_PARAMETER_DISPLAY_NAME] as? String)
//        XCTAssertEqual(TEST_PROFILE_URL?.absoluteString, responseData?[RESPONSE_PARAMETER_PROFILE_URL] as? String)
//        XCTAssertEqual(TEST_PICTURE_URL?.absoluteString, responseData?[RESPONSE_PARAMETER_PICTURE_URL] as? String)
//    }
//
//    func test_UserServices_ListUsers()
//    {
//        let listUsers = UsersServices.ListUsers()
//        listUsers.limit = "2"
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var usersData:[[AnyHashable:Any]]?
//
//        services.ams.usersServices(listUsers) { (response) in
//            let responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            usersData = responseData?[RESPONSE_PARAMETER_USERS] as? [[AnyHashable:Any]]
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//        XCTAssertEqual(2, usersData?.count)
//    }
//
//    func test_UserServices_ListUsersMore()
//    {
//        let listUsersMore = UsersServices.ListUsersMore()
//        listUsersMore.limit = "5"
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//        var usersData = NSArray()
//
//        services.ams.usersServices(listUsersMore) { (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            usersData = responseData?[RESPONSE_PARAMETER_USERS] as? NSArray ?? NSArray()
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//        XCTAssertEqual(5, usersData.count)
//    }
//
    func test_UserServies_ListMessagesByUsers()
    {
        let listMessagesByUser = UsersServices.ListMessagesByUser()
        listMessagesByUser.limit = "4"
        listMessagesByUser.userId = TEST_USERID

        var responseData:[AnyHashable:Any]?
        let expectation = self.expectation(description: Expectation_Description)

        services.ams.usersServices(listMessagesByUser) { (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert((responseData?.count ?? 0) > 0)
    }
//
//    func test_UserServies_SearchUsersByHandle()
//    {
//        let searchUsersByHandle = UsersServices.SearchUsersByHandle()
//        searchUsersByHandle.handle = TEST_HANDLE
//
//        var users:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.usersServices(searchUsersByHandle) { (response) in
//            let responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            users = responseData?[RESPONSE_PARAMETER_USERS] as? [[AnyHashable:Any]]
//
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssert((users?.count ?? 0) > 0)
//    }
//
//    func test_UserServies_SearchUsersByName()
//    {
//        let searchUsersByName = UsersServices.SearchUsersByName()
//        searchUsersByName.name = name
//
//        var users:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.usersServices(searchUsersByName) { (response) in
//            let responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            users = responseData?[RESPONSE_PARAMETER_USERS] as? [[AnyHashable:Any]]
//
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssert((users?.count ?? 0) > 0)
//    }
//
//    func test_UserServies_SearchUsersByUserId()
//    {
//        let searchUsersByUserId = UsersServices.SearchUsersByUserId()
//        searchUsersByUserId.userId = TEST_USERID
//
//        var users:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.usersServices(searchUsersByUserId) { (response) in
//            let responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            users = responseData?[RESPONSE_PARAMETER_USERS] as? [[AnyHashable:Any]]
//
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssert((users?.count ?? 0) > 0)
//    }
//
//    func test_UserServies_CreateWebhooksServices()
//    {
//        let createReplaceWebhook = WebhooksServices.CreateReplaceWebhook()
//        createReplaceWebhook.label = "Webhook Demonstration"
//        createReplaceWebhook.url = URL(string: "http://shaped-entropy-259212.appspot.com")
//        createReplaceWebhook.enabled = false
//        createReplaceWebhook.type = "prepublish"
//        createReplaceWebhook.events = ["exit", "roomopened", "roomclosed", "purge"]
//
//
//        var users:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.webhooksServices(createReplaceWebhook, completionHandler: { (response) in
//            let responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            users = responseData?[RESPONSE_PARAMETER_USERS] as? [[AnyHashable:Any]]
//
//            expectation.fulfill()
//        })
//
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssert((users?.count ?? 0) > 0)
//    }
//
//    func test_UserServies_ListWebhooksServices()
//    {
//        let listWebhooks = WebhooksServices.ListWebhooks()
//
//        var webhooks:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.webhooksServices(listWebhooks, completionHandler: { (response) in
//            let responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            webhooks = responseData?["webhooks"] as? [[AnyHashable:Any]]
//
//            expectation.fulfill()
//        })
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(webhooks != nil)
//    }
//
//    func test_UserServies_UpdateWebhooksServices()
//    {
//        let updateWebhook = WebhooksServices.UpdateWebhook()
//        updateWebhook.webhookId = TEST_WEBHOOK_ID
//        updateWebhook.label = "Webhook Demonstration"
//        updateWebhook.url = URL(string: "http://shaped-entropy-259212.appspot.com")
//        updateWebhook.enabled = false
//        updateWebhook.type = "prepublish"
//        updateWebhook.events = ["exit", "roomopened", "roomclosed", "purge"]
//
//
//        var responseData:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.webhooksServices(updateWebhook, completionHandler: { (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [[AnyHashable : Any]]
//
//            expectation.fulfill()
//        })
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssert(responseData != nil)
//    }
//
//    func test_UserServies_DeleteWebhooksServices()
//    {
//        let deleteWebhooks = WebhooksServices.DeleteWebhook()
//
//        deleteWebhooks.webhookId = TEST_WEBHOOK_ID
//
//        var responseData:[[AnyHashable:Any]]?
//        let expectation = self.expectation(description: Expectation_Description)
//
//        services.ams.webhooksServices(deleteWebhooks, completionHandler: { (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [[AnyHashable:Any]]
//
//            expectation.fulfill()
//        })
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(responseData != nil)
//    }
//
    func test_ModerationServices_BanUser()
    {
        let banUser = ModerationServices.BanUser()
        banUser.userid = TEST_USERID
        banUser.banned = TEST_BANNED

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(banUser){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ModerationServices_RestoreUser()
    {
        let restoreUser = ModerationServices.RestoreUser()
        restoreUser.userid = TEST_USERID
        restoreUser.banned = TEST_BANNED

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(restoreUser){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ModerationServices_PurgeUserMessages()
    {
        let purgeUserMessages = ModerationServices.PurgeUserMessages()
        purgeUserMessages.userid = TEST_USERID
        purgeUserMessages.command = TEST_COMMAND
        purgeUserMessages.chatroomid = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(purgeUserMessages){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ModerationServices_DeleteAllEventsInRoom()
    {
        let deleteAllEventsInRoom = ModerationServices.DeleteAllEventsInRoom()
        deleteAllEventsInRoom.userid = TEST_USERID
        deleteAllEventsInRoom.command = TEST_COMMAND
        deleteAllEventsInRoom.chatroomid = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(deleteAllEventsInRoom){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ModerationServices_RemoveMessage()
    {
        let removeMessage = ModerationServices.RemoveMessage()
        removeMessage.chatMessageId = TEST_CHAT_MESSAGE_ID
        removeMessage.chatRoomId = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(removeMessage){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ModerationServices_ReportMessage()
    {
        let reportMessage = ModerationServices.ReportMessage()
        reportMessage.chatMessageId = TEST_CHAT_MESSAGE_ID
        reportMessage.chatRoomId = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(reportMessage){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ModerationServices_ApproveMessage()
    {
        let approveMessage = ModerationServices.ApproveMessage()
        approveMessage.chatMessageId = TEST_CHAT_MESSAGE_ID
        approveMessage.chatRoomId = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.moderationServies(approveMessage){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

//    func test_ChatRoomsServices_CreateRoom()
//    {
//        let createRoom = ChatRoomsServices.CreateRoom()
//        createRoom.name = TEST_DISPLAY_NAME
//        createRoom.slug = TEST_SLUG
//        createRoom.description = TEST_CHAT_ROOM_ID
//        createRoom.enableactions = TEST_ENABLEACTIONS
//        createRoom.enableenterandexit = TEST_ENABLEENTERANDEXIT
//        createRoom.roomisopen = TEST_ROOMISOPEN
//        createRoom.userid = TEST_USERID
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//
//        services.ams.chatRoomsServices(createRoom){ (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            print(response)
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(responseData != nil)
//    }
//
//    func test_ChatRoomsServices_UpdateRoom()
//    {
//        let updateRoom = ChatRoomsServices.UpdateRoom()
//        updateRoom.name = TEST_DISPLAY_NAME
//        updateRoom.slug = TEST_SLUG
//        updateRoom.description = TEST_DESCRIPTION
//        updateRoom.enableactions = TEST_ENABLEACTIONS
//        updateRoom.enableenterandexit = TEST_ENABLEENTERANDEXIT
//        updateRoom.roomisopen = TEST_ROOMISOPEN
//        updateRoom.userid = TEST_USERID
//        updateRoom.roomid = TEST_CHAT_ROOM_ID
//        updateRoom.throttle = TEST_THROTTLE
//
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//
//        services.ams.chatRoomsServices(updateRoom){ (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            print(response)
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(responseData != nil)
//    }
//
//    func test_ChatRoomsServices_UpdateRoomCloseARoom()
//    {
//        let updateRoomCloseARoom = ChatRoomsServices.UpdateRoomCloseARoom()
//        updateRoomCloseARoom.roomisopen = TEST_ROOMISOPEN
//        updateRoomCloseARoom.roomid = TEST_CHAT_ROOM_ID
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//
//        services.ams.chatRoomsServices(updateRoomCloseARoom){ (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            print(response)
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(responseData != nil)
//    }
//
    func test_ChatRoomsServices_ListRooms()
    {
        let listRooms = ChatRoomsServices.ListRooms()

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[[AnyHashable:Any]]?

        services.ams.chatRoomsServices(listRooms){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [[AnyHashable:Any]]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }
//
    func test_ChatRoomsServices_ListRoomParticipants()
    {
        let listRoomParticipants = ChatRoomsServices.ListRoomParticipants()
        listRoomParticipants.cursor = TEST_CURSOR
        listRoomParticipants.roomid = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(listRoomParticipants){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ChatRoomsServices_JoinRoomAuthenticatedUser()
    {
        let joinRoomAuthenticatedUser = ChatRoomsServices.JoinRoomAuthenticatedUser()
        joinRoomAuthenticatedUser.userid = TEST_USERID
        joinRoomAuthenticatedUser.handle = TEST_HANDLE
        joinRoomAuthenticatedUser.displayname = TEST_DISPLAY_NAME
        joinRoomAuthenticatedUser.pictureurl = TEST_PICTURE_URL
        joinRoomAuthenticatedUser.profileurl = TEST_PROFILE_URL
        joinRoomAuthenticatedUser.roomid = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(joinRoomAuthenticatedUser){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ChatRoomsServices_JoinRoomAnonymousUser()
    {
        let joinRoomAnonymousUser = ChatRoomsServices.JoinRoomAnonymousUser()
        joinRoomAnonymousUser.roomid = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(joinRoomAnonymousUser){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ChatRoomsServices_GetUpdates()
    {
        let getUpdates = ChatRoomsServices.GetUpdates()
        getUpdates.roomId = TEST_CHAT_ROOM_ID

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[[AnyHashable:Any]]?

        services.ams.chatRoomsServices(getUpdates){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [[AnyHashable:Any]]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }
//
//    func test_ChatRoomsServices_GetUpdatesMore()
//    {
//        let getUpdatesMore = ChatRoomsServices.GetUpdatesMore()
//        getUpdatesMore.roomIdOrLabel = TEST_ROOM_ID_OR_LABEL
//        getUpdatesMore.eventid = TEST_EVENT_ID
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//
//        services.ams.chatRoomsServices(getUpdatesMore){ (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            print(response)
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(responseData != nil)
//    }
//
    func test_ChatRoomsServices_ExecuteChatCommand()
    {
        let executeChatCommand = ChatRoomsServices.ExecuteChatCommand()
        executeChatCommand.roomId = TEST_CHAT_ROOM_ID
        executeChatCommand.command = TEST_COMMAND
        executeChatCommand.userid = TEST_USERID
        executeChatCommand.customtype = TEST_CUSTOM_EVENT
        executeChatCommand.customid = TEST_CUSTOM_ID
        executeChatCommand.custompayload = TEST_CUSTOM_PAYLOAD

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(executeChatCommand){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ChatRoomsServices_ExecuteDanceAction()
    {
        let executeDanceAction = ChatRoomsServices.ExecuteDanceAction()
        executeDanceAction.roomId = TEST_CHAT_ROOM_ID
        executeDanceAction.command = TEST_COMMAND
        executeDanceAction.userid = TEST_USERID
        executeDanceAction.customtype = TEST_CUSTOM_EVENT
        executeDanceAction.customid = TEST_CUSTOM_ID
        executeDanceAction.custompayload = TEST_CUSTOM_PAYLOAD

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(executeDanceAction){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ChatRoomsServices_ReplyToAMessage()
    {
        let replyToAMessage = ChatRoomsServices.ReplyToAMessage()
        replyToAMessage.roomId = TEST_CHAT_ROOM_ID
        replyToAMessage.command = TEST_COMMAND
        replyToAMessage.userid = TEST_USERID
        replyToAMessage.replyto = TEST_REPLY_TO

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(replyToAMessage){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }


    func test_ChatRoomsServices_ReactToAMessageLike()
    {
        let reactToAMessageLike = ChatRoomsServices.ReactToAMessageLike()
        reactToAMessageLike.roomId = TEST_CHAT_ROOM_ID
        reactToAMessageLike.roomNewestEventId = TEST_ROOM_NEWEST_EVENT_ID
        reactToAMessageLike.userid = TEST_USERID
        reactToAMessageLike.reaction = TEST_REACTION
        reactToAMessageLike.reacted = TEST_REACTED

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(reactToAMessageLike){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }

    func test_ChatRoomsServices_ExecuteAdminCommand()
    {
        let executeAdminCommand = ChatRoomsServices.ExecuteAdminCommand()
        executeAdminCommand.roomId = TEST_CHAT_ROOM_ID
        executeAdminCommand.command = TEST_COMMAND
        executeAdminCommand.userid = TEST_USERID
        executeAdminCommand.customtype = TEST_CUSTOM_EVENT
        executeAdminCommand.customid = TEST_CUSTOM_ID
        executeAdminCommand.custompayload = TEST_CUSTOM_PAYLOAD

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(executeAdminCommand){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertEqual(responseData?[RESPONSE_PARAMETER_CODE] as? Int64 , 200)
        XCTAssertTrue(responseData != nil)
    }
//
//    func test_ChatRoomsServices_ExitRoom()
//    {
//        let exitRoom = ChatRoomsServices.ExitRoom()
//        exitRoom.roomId = TEST_CHAT_ROOM_ID
//        exitRoom.userid = TEST_USERID
//
//        let expectation = self.expectation(description: Expectation_Description)
//        var responseData:[AnyHashable:Any]?
//
//        services.ams.chatRoomsServices(exitRoom){ (response) in
//            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
//            print(response)
//            expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        XCTAssertTrue(responseData != nil)
//    }
//
    func test_ChatRoomsServices_GetRoomDetails()
    {
        let getRoomDetails = ChatRoomsServices.GetRoomDetails()
        getRoomDetails.roomIdOrSlug = TEST_ROOM_ID_OR_SLUG

        let expectation = self.expectation(description: Expectation_Description)
        var responseData:[AnyHashable:Any]?

        services.ams.chatRoomsServices(getRoomDetails){ (response) in
            responseData = response[RESPONSE_PARAMETER_DATA] as? [AnyHashable:Any]
            print(response)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        XCTAssertTrue(responseData != nil)
    }
////
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}


//sendAdvertisement
//sendGoal


//not working
//ExecuteAdminCommand
